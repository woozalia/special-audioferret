<?php
/*
  NAME: SpecialAudioFerret
  PURPOSE: Special page for accessing AudioFerret
  AUTHOR: Woozle (Nick) Staddon
  VERSION:
    2009-03-10 0.0 (Wzl) Started writing
    2009-03-12 0.0 (Wzl) everything working except download/queue
    2009-04-06 0.1 (Wzl) Basic features working; minor fix to prevent use of undeclared variables
    2009-05-29 0.2 (Wzl) Now using /key:val/ internal URL format
	allows direct linking, but makes it more difficult to embed full functionality
	maybe we should support both URL formats? if embedded, use ?key=val, otherwise use /key:val/?
    2009-06-21 0.3 (Wzl) After queueing title, shows title's album/artist listing (so you don't have to backtrack)
    2009-06-22 0.4 (Wzl) Added "most recently played" list, which required some serious table reformatting
    2009-12-31 0.41 (Wzl) option for favicon
    2010-04-05 0.5 (Wzl) separate page for each function
    2012-01-10 0.51 (Wzl) minor bugfixes due to API changes (mostly data.php)
*/
$wgSpecialPages['AudioFerret'] = 'SpecialAudioFerret'; # Let MediaWiki know about your new special page.
$wgExtensionCredits['other'][] = array(
        'name' => 'Special:AudioFerret',
	'url' => 'http://htyp.org/AudioFerret',
        'description' => 'special page for AudioFerret access',
        'author' => 'Woozle (Nick) Staddon',
	'version' => '0.51 2012-01-10 alpha'
);
$dir = dirname(__FILE__) . '/';
$wgAutoloadClasses['AudioFerret'] = $dir . 'AudioFerret.php'; # Location of the SpecialMyExtension class (Tell MediaWiki to load this file)
$wgExtensionMessagesFiles['AudioFerret'] = $dir . 'AudioFerret.i18n.php'; # Location of a messages filename

clsLibMgr::Add('admin',		KFP_MW_LIB.'/admin.php',__FILE__,__LINE__);

clsLibMgr::AddClass('clsAdminData_helper'	,'admin');

/*
if (isset($kfpWzlLibs)) {
    $fpLibData = $kfpWzlLibs;
} else {
    $fpLibData = '';
}
require_once $fpLibData.'data.php';
*/
if (!defined('kfsLib_Data')) {
    if (defined('kfpLib')) {
	define('kfsLib_Data', kfpLib.'/data.php');
    } else {
	define('kfsLib_Data','data.php');	// assume it's on the path
    }
}
require_once kfsLib_Data;
if (!defined('LIBMGR')) {
    require('libmgr.php');
}
//clsLibMgr::$doDebug = TRUE;
clsLibMgr::Add('menus',		KFP_MW_LIB.'/menu.php',__FILE__,__LINE__);
clsLibMgr::Add('richtext',	KFP_MW_LIB.'/richtext.php',__FILE__,__LINE__);
clsLibMgr::Load('menus'		,__FILE__,__LINE__);
clsLibMgr::Load('richtext'	,__FILE__,__LINE__);

/*
function wfSpecialAudioFerret() {
// This registers the page's class. I think.
	global $wgRequest, $spgSpecialPage;
echo "got to here";
	$spgSpecialPage = new SpecialAudioFerret($wgRequest);
}
*/
require_once( $wgScriptPath.'includes/SpecialPage.php' );
//require_once( $wgScriptPath.'includes/EditPage.php' );
if (!defined('KS_CHAR_URL_ASSIGN')) {
    define('KS_CHAR_URL_ASSIGN',':');	// character used for encoding values in wiki-internal URLs
}
if (!defined('KS_KEY_PAGENAME')) {
    define('KS_KEY_PAGENAME','do');
}

class SpecialAudioFerret extends SpecialPageApp {

    public function __construct() {
	  global $wgMessageCache;
	  global $spgSpecialPage;
		global $vgPage;

	  $spgSpecialPage = $this;

	  parent::__construct( 'AudioFerret' );
		$vgPage = $this;

	  $this->includable( TRUE );	// sure, why not?
	  //$wgMessageCache->addMessage('audioferret', 'AudioFerret access');

    }
    /*
    FUNCTION: SelfLinkID
    PURPOSE: self-link for always passing an ID
    */
    function SelfLinkID($iDo,$iKey,$iVal,$iText) {
    //    return '[[{{FULLPAGENAME}}/do'.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	//return '[[Special:AudioFerret/'.KS_KEY_PAGENAME.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	//return '[[{{FULLPAGENAME}}/page'.KS_CHAR_URL_ASSIGN.$iPage.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	return '[[Special:'.$this->name().'/'.KS_KEY_PAGENAME.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
    }
    function execute( $par ) {
	global $wgRequest, $wgOut, $wgUser;
	global $vgPage;
	global $doDebug;
	global $AudioFerretOptions;

	$vgPage->UseHTML();

	if (isset($AudioFerretOptions['favicon'])) {
	    $wgOut->addHeadItem('faviconlink', '<link rel="icon" type="image/vnd.microsoft.icon" href="'.$AudioFerretOptions['favicon'].'">' );
	}

	$this->setHeaders();

// Open the AudioFerret database
	$dbAF = new clsAFData(KS_DB_AUFERRET);
	$dbAF->Open();

// check for action requests
	$this->GetArgs($par);

	$strDo = $this->Arg('page');
	$id = $this->Arg('id');
	$doDebug = $this->Arg('debug');
	$noHdr = $this->Arg('-hdr');

	$idArtist = NULL;
	$idAlbum = NULL;
	$out = NULL;
	switch ($strDo) {
	  case 'artist':
	    $idArtist = $id;
	    $objArtist = $dbAF->Artists()->GetItem($id);
	    $strArtist = $objArtist->Value('name');
	    if (!$noHdr) {
		$out .= '<h2>'.$strArtist.'</h2>';
	    }
	    $outAlb = $dbAF->GetAlbums($idArtist);
	    $outSng = $dbAF->GetTitles($idArtist);
	    if (!empty($outAlb)) {
		$out .= '<h3>Albums</h3>'.$outAlb;
	    }
	    if (!empty($outSng)) {
		$out .= '<h3>Singles</h3>'.$outSng;
	    }
	    break;
	  case 'album':
	    $idAlbum = $id;
	    $objAlbum = $dbAF->Albums()->GetItem($id);
	    $objArtist = $dbAF->Artists()->GetItem($objAlbum->ArtistID());
	    $strArtist = $objArtist->Value('name');
	    $strAlbum = $objAlbum->Value('name');
    	    if (!$noHdr) {
		$out .= '<h3>'.$objArtist->AdminLink($strArtist).': <i>'.$strAlbum.'</i></h3>';
	    }
	    $out .= $dbAF->GetTitles(NULL,$idAlbum);
	    break;
	  case 'title':
	    $idTitle = $id;
	    $out .= $dbAf->RenderTitle($idTitle);
	    break;
	  case 'queue':
	    $idTitle = $id;
	    $dbAF->QueueTitle($id);
	    break;
	  case 'grab':
	    $dbAF->GrabTitle($id);
	    break;
	  default:
	    $out .= $dbAF->GetArtists($idArtist);
	}
/**/
/*
	if (isset($idTitle)) {
	  if (!is_null($idTitle)) {
	      $sql = 'SELECT * FROM Titles WHERE id='.$idTitle;
	      $objTitle = $dbAF->DataSet($sql,'clsTitle');
	      $objTitle->NextRow();
	      $idAlbum = $objTitle->album_id;
	  }
	}
	if (!is_null($idAlbum)) {
	    $sql = 'SELECT * FROM Albums WHERE id='.$idAlbum;
	    $objAlbum = $dbAF->DataSet($sql,'clsAlbum');
	    $objAlbum->NextRow();
	    $strAlbum = $objAlbum->NameLink();
	    $idArtist = $objAlbum->idartist;
	}
	if (!is_null($idArtist)) {
	    $sql = 'SELECT * FROM Artists WHERE id='.$idArtist;
	    $objArtist = $dbAF->DataSet($sql,'clsArtist');
	    $objArtist->NextRow();
	    $strArtist = $objArtist->NameLink();
	}
// collect data for the display
	$outArtists = $dbAF->GetArtists($idArtist);
	if (!is_null($idArtist)) {
	    $outAlbums = $dbAF->GetAlbums($idArtist,$idAlbum);
	    $outTitles = $dbAF->GetTitles($idArtist,$idAlbum);
	} else {
	    $outAlbums = NULL;
	    $outTitles = NULL;
	}
	
	$outPlayed = $dbAF->ListPlayed(10);

// display the current listing(s)
	$out = $doDebug?"DEBUG mode\n":''; 
	$out .= "__NOTOC__\n{| border=0";
	$outHtArtist = '';
	$outHtAlbum = '';
	if ($idArtist != '') {
	    $outHtArtist = "'''$strArtist'''\n==Albums==\n";
	    if ($idAlbum) {
		$outHtAlbum = "'''$strAlbum'''\n==Tracks==\n";
	    }
	}
	$outRtPanel = "\n{|\n|-\n|\n{|\n|-\n| valign=top | $outHtArtist$outAlbums \n| valign=top | $outHtAlbum$outTitles\n|}\n|}";
	$outRtPanel .= "\n==most recently played==\n$outPlayed";

	$out .= "\n|-\n| valign=top |\n==Artists==\n$outArtists";
	$out .= "\n| valign=top | $outRtPanel";

	$out .= "\n|}";

	global $wgParserConf;
	$objParser = new StubObject( 'wgParser', $wgParserConf['class'], array( $wgParserConf ) );
*/

	//$wgOut->addWikiText($out,TRUE);	$out = '';
	$wgOut->addHTML($out);	$out = '';
	$dbAF->Shut();
  }
    /*-----
      PURPOSE: Parses variable arguments from the URL
	The URL is formatted as a series of arguments /arg=val/arg=val/..., so that we can always refer directly
	  to any particular item as a wiki page title while also not worrying about hierarchy/order.
    */
/*
    private function GetArgs($par) {
	$args_raw = split('/',$par);
	foreach($args_raw as $arg_raw) {
	    if (strpos($arg_raw,KS_CHAR_URL_ASSIGN) !== FALSE) {
		list($key,$val) = split(KS_CHAR_URL_ASSIGN,$arg_raw);
		$this->args[$key] = $val;
	    } else {
		$this->args[$arg_raw] = TRUE;
	    }
	}
    }
*/
}

class clsAFData extends clsDatabase {
/*==============
| STATIC SECTION
*/
    //const ksTbl_Artists = 'Artists';
    //const ksTbl_Albums = 'Albums';
    //const ksTbl_Titles = 'Titles';
    const ksTbl_Files = 'Files';
    const ksTbl_Queue = 'Tracks_Queued';
    const ksTbl_Played = 'Tracks_Played';

    private static $objArtists;
    private static $objAlbums;
    private static $objTitles;
    private static $objFiles;
/*===============
| DYNAMIC SECTION
*/
/*
    protected function Make($iName) {
	if (!isset($this->arObjs[$iName])) {
	    $this->arObjs[$iName] = new $iName($this);
	}
	return $this->arObjs[$iName];
    }
*/
    public function Artists() {
	return $this->Make('clsAuFerArtists');
    }
    public function Albums() {
	return $this->Make('clsAuFerAlbums');
    }
    public function Titles() {
	return $this->Make('clsAuFerTitles');
    }
/*
    public function Artists() {
	if (NoObject(self::$objArtists)) {
	    $obj = new clsTable($this,self::ksTbl_Artists,'id','clsArtist');
	      $obj->Name(self::ksTbl_Artists);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsArtist');
	    self::$objArtists = $obj;
	}
	return self::$objArtists;
    }
    public function Albums() {
	if (NoObject(self::$objAlbums)) {
	    $obj = new clsTable($this);
	      $obj->Name(self::ksTbl_Albums);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsAlbum');
	      self::$objAlbums = $obj;
	}
	return self::$objAlbums;
    }
    public function Titles() {
	if (NoObject(self::$objTitles)) {
	    $obj = new clsTable($this);
	      $obj->Name(self::ksTbl_Titles);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsTitle');
	      self::$objTitles = $obj;
	}
	return self::$objTitles;
    }
*/
    public function Files() {
	if (NoObject(self::$objFiles)) {
	    $obj = self::$objFiles = new clsTable($this);
	      $obj->Name(self::ksTbl_Files);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsFile');
	}
	return self::$objFiles;
    }

    public function GetArtists($iCurrent = NULL) {
	$out = $this->Artists()->RenderList();
	return $out;
    }
    public function GetAlbums($iArtist, $iCurrent = NULL) {
	$out = $this->Albums()->RenderList($iArtist,$iCurrent);
	return $out;
    }
    public function GetTitles($iArtist, $iAlbum = NULL,$iNone=NULL) {
	$out = $this->Titles()->RenderList($iArtist,$iAlbum,$iNone);
	return $out;
    }
    /*-----
      ACTION: Display information about the title
    */
    public function RenderTitle($iTitle) {
    }
    /*-----
      ACTION: Add title to playing queue
    */
    public function QueueTitle($iTitle) {
	global $wgUser, $wgOut, $wgSitename;

	$sql = 'INSERT INTO `'.self::ksTbl_Queue.'` (id_title,when_requested,user,station) VALUES('
	  .$iTitle.',NOW(),"'
	  .$wgUser->getName()
	   .'","'.$wgSitename.'")';
	$this->Exec($sql);
	$wgOut->addWikiText('Added '.$this->Titles()->GetItem($iTitle)->LongName());
    }
    public function GrabTitle($iTitle) {
    /*
      ACTION: download audio file for the given title
    */
	global $wgRequest, $wgOut;
	global $doDebug;

	define( 'MW_NO_OUTPUT_COMPRESSION', 1 );	// this is absolutely vital or the download won't work

	$objTitle = $this->Titles()->GetItem($iTitle);
	$objFile = $this->Files()->GetItem($objTitle->Value('id_file'));

	$fsRel = str_replace('\\', '/', $objFile->Value('filespec'));	// db was originally loaded in Windows
	$fsFull = KFP_AUFERRET_AUDIO.$fsRel;
	$fsEscaped = str_replace ( '"','\\"',$fsFull );

	$strMime = mime_content_type($fsFull);
	$fsNameAs = $objTitle->FriendlySpec('<artist><sep><album><sep><track> <title>.<ext>',' - ');
	$fsNameAs = str_replace ( '"',"''",$fsNameAs);

/*
	$wgOut->addWikiText(' Content-type: '.$strMime);
	$wgOut->addWikiText(' Content-Disposition: attachment; filename="'.$fsNameAs.'"');
	$wgOut->addWikiText(' FILENAME ESCAPED:'.$fsEscaped);
/*/
//	    $wgRequest->response()->header('Content-type: '.$strMime);
//	    $wgRequest->response()->header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');

	//header('Content-type: text/html');
	//echo "\nSending $fsNameAs...";
// */
	$stat = @stat( $fsFull );
	if ( !$stat ) {
	    $wgOut->addWikiText("File $fsFull doesn't seem to be available.");
	} else {
	    $canGrab = $this->CanGrab();
	    //$canGrab = TRUE;
	    if ($canGrab) {
		if ( headers_sent() ) {
			$wgOut->addWikiText ("Headers already sent; can't send file.");
			return;
		}
		//ob_end_clean();
		if ($doDebug) {
		  $wgOut->addWikiText('* Content-type: '.$strMime,TRUE);
		  $wgOut->addWikiText('* Content-Disposition: attachment; filename="'.$fsNameAs.'"',TRUE);
		  $wgOut->addWikiText('* Content-Length: ' . $stat['size'] );
		  $wgOut->addWikiText('* source filespec: ' . $fsFull );
		} else {
		  header('Content-Description: File Transfer');
		  header('Content-Transfer-Encoding: binary');
		  header('Expires: 0');
		  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		  header('Pragma: public');
		  header('Content-type: '.$strMime);
		  //header('Content-Type: application/octet-stream');
		  header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');
		  header('Content-Length: ' . $stat['size'] );
// this gets the download dialogue with a reasonable filesize, but no file:
		  ob_end_flush();
//		readfile( $fsFull );
// this is sample code from php.net:
//		ob_clean();
//		flush();
		
		  readfile($fsFull);
		  exit;
		}
//	    echo "\n";
	    //passthru ( 'cat "'.$fsEscaped.'"');
//	    header('Content-type: text/html');
	    //header('Content-type: '.$strMime);
	    //header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');
	    //die();
/**/
	    } else {
	      $wgOut->AddWikiText('File information:<br>');
	      $wgOut->AddWikiText("\n* Content-type: $strMime");
	      $wgOut->AddWikiText("\n* Content-Disposition: attachment; filename=\"".$fsNameAs.'"');
	      $wgOut->AddWikiText("\n* Content-Length: ".$stat['size'] );
  		//readfile($fsFull);
	    }
	}
    }
    public function ListQueued() {
	$sql = 'SELECT * FROM '.self::ksTbl_Queue.' ORDER BY id';
	$objRow = $this->DataSet($sql);
	if ($objRow->hasRows()) {
	    $out = "\n{|";
	    while ($objRow->NextRow()) {
		$objTitle = $objRow->TitleObj();
		$out .= "\n|-\n|".$objTitle->LongName().' || '.$objRow->when_requested;
	    }
	    $out .= "\n|}";
	    return $out;
	} else {
	    return 'no tracks played yet';
	}
    }
    public function ListPlayed($iQty) {
	$sql = 'SELECT * FROM '.self::ksTbl_Played.' ORDER BY id DESC LIMIT '.$iQty;
	$objRow = $this->DataSet($sql,'clsRequest');
	if ($objRow->hasRows()) {
	    $out = "\n{|";
	    while ($objRow->NextRow()) {
		$objTitle = $objRow->TitleObj();
		$out .= "\n|-\n|".$objTitle->LongName().' || '.$objRow->when_started;
	    }
	    $out .= "\n|}";
	    return $out;
	} else {
	    return NULL;
	}
    }
    public function CanGrab() {
	global $wgUser;

  	return $wgUser->isAllowed('editinterface');
    }
    public function CanQueue() {
	global $wgUser;

	return $wgUser->isLoggedIn();
    }
}
class clsAuFerArtists extends clsTable_key_single {
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Artist');
	  $this->KeyName('id');
	  $this->ClassSng('clsAuFerArtist');
	  $this->ActionKey('artist');
    }
    public function RenderList() {
	  global $vgPage;

	  $sql = 'SELECT * FROM qryArtists_index ORDER BY name';
	  $objData = $this->objDB->DataSet($sql,'clsAuFerArtist');
	  $objData->Table = $this;

	  if ($objData->hasRows()) {
	      $out = '';
	      while ($objData->NextRow()) {
		  if ($out != '') {
		      $out .= '<br>';
		  }
		  $out .= $objData->AdminLink($objData->Value('name'));
	      }
	  } else {
	      $out = "Can't retrieve artist list - check database spec!";
	  }
	  return $out;
    }
}
class clsAuFerArtist extends clsRecs_key_single {
// BOILERPLATE section //

    public function AdminLink($iText=NULL,$iPopup=NULL,array $iarArgs=NULL) {
	return clsAdminData_helper::_AdminLink($this,$iText,$iPopup,$iarArgs);
    }

// DYNAMIC section //
}

class clsAuFerAlbums extends clsTable_key_single {
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Album');
	  $this->KeyName('id');
	  $this->ClassSng('clsAuFerAlbum');
	  $this->ActionKey('album');
    }
    public function RenderList($iArtist, $iCurrent = NULL,$iNone=NULL) {
	global $vgPage;

	$vgPage->UseHTML();
	//$sql = 'SELECT * FROM '..' WHERE idartist='.$iArtist.' ORDER BY sort';
	$objData = $this->GetData('idartist='.$iArtist,NULL,'sort');
	if ($objData->hasRows()) {
	    $out = '';
	    while ($objData->NextRow()) {
		$strName = $objData->Value('name');
		// square brackets [] confuse the wiki parser; change them to something else
		if ($strName == '') {
		    $strName = '??';
		} else {
		    $strName = str_replace  ('[','&lt;',$strName);
		    $strName = str_replace  (']','&gt;',$strName);
		}
//		 $strName = 'X'.$strName.'X';
		if ($out != '') {
		    $out .= '<br>';
		}
		if ($objData->Value('id') == $iCurrent) {
		    $out .= "<b>$strName</b>";
		} else {
		    //$out .= WikiSelfLink('artist='.$iArtist.'&album='.$objData->id,$strName);
		    //$out .= $spgSpecialPage->SelfLinkID('album','id',$objData->id,$objData->name);
		    $out .= $objData->AdminLink($objData->Value('name'));
		}
	    }
	} else {
	    $out = $iNone;
	}
	return $out;
    }
}
class clsAuFerAlbum extends clsRecs_key_single {
// BOILERPLATE section //

    public function AdminLink($iText=NULL,$iPopup=NULL,array $iarArgs=NULL) {
	return clsAdminData_helper::_AdminLink($this,$iText,$iPopup,$iarArgs);
    }

// DYNAMIC section //

    public function ArtistID() {
	return $this->Value('idartist');
    }
}

class clsAuFerTitles extends clsTable_key_single {
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Title');
	  $this->KeyName('id');
	  $this->ClassSng('clsAuFerTitle');
	  $this->ActionKey('title');
    }
    public function RenderList($iArtist, $iAlbum = NULL,$iNone=NULL) {
	global $vgPage,$vgOut;

	$vgPage->UseHTML();

	if (!empty($iAlbum)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE album_id='.$iAlbum.' ORDER BY track';
	    $sqlFilt = 'album_id='.$iAlbum;
	} elseif (!empty($iArtist)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE artist_id='.$iArtist.' ORDER BY track';
	    $sqlFilt = 'artist_id='.$iArtist;
	}
//	$objData = $this->DataSet($sql);

	$objData = $this->GetData($sqlFilt);
	$objData->Table = $this;

  	$canGrab = $this->objDB->CanGrab();
	$canQueue = $this->objDB->CanQueue();

	if ($objData->hasRows()) {
	    $out = "\n<table>";
	    while ($objData->NextRow()) {
		$strTrack = $objData->Value('track');
		$strName = $objData->Value('name');
		if ($strTrack == '') {
		    $strTrack = '?';
		}
		$out .= "<tr><td align=right><b>$strTrack</b></td><td>$strName</td><td>";
		//$out .= ' || ';
		$arLink['id'] = $objData->KeyValue();
		if ($canQueue) {
		    //$out .= $spgSpecialPage->SelfLinkID('queue','id',$objData->id,'queue');
		    $arLink['page'] = 'queue';
		    $out .= $vgOut->SelfLink($arLink,'queue','add track to the playing queue');
		}
		if ($canGrab) {	// only show download links for admins
		    if ($canQueue) {
			$out .= '|';
		    }
		    //$out .= ' '.WikiSelfLink('do=grab&track='.$objData->id,'grab');
		    //$out .= $spgSpecialPage->SelfLinkID('grab','id',$objData->id,'grab');
		    $arLink['page'] = 'grab';
		    $out .= $vgOut->SelfLink($arLink,'grab','download the file for this track');
		}
		$out .= '</td></tr>';
	    }
	    $out .= "\n</table>";
	} else {
/*
	    if ($iAlbum) {
		$out = "No titles for this album";
	    } else {
		$out = "No singles for this artist";
	    }
*/
	    $out = $iNone;
	}
	return $out;
    }
}
class clsAuFerTitle extends clsRecs_key_single {
    private $vArtists;

    public function FriendlySpec($iTemplate,$iSep) {
	$oAlbum = $this->AlbumObj();
	if (is_object($oAlbum)) {
	    $sAlbum = $oAlbum->Value('name');
	} else {
	    $sAlbum = '';	// no album for this track
	}

	$out = $iTemplate;
	$out = str_replace('<artist>',$this->ArtistObj()->Value('name'),	$out);
	$out = str_replace('<album>',$sAlbum,	$out);
	$out = str_replace('<title>',$this->Value('name'),		$out);
	$out = str_replace('<track>',$this->Value('track'),		$out);
	$out = str_replace('<ext>',$this->FileObj()->Extn(),	$out);
	$out = str_replace('<sep>',$iSep,$out);
	return $out;
    }
    public function LongName() {
	$out = '&ldquo;'.$this->Value('name').'&rdquo; by '.$this->ArtistObj()->Value('name');
	return $out;
    }
    public function ActiveArtist() {
	$idArtist = $this->Value('artist_id');
	if (is_null($idArtist)) {
	    return $this->AlbumObj()->Value('idartist');
	} else {
	    return $idArtist;
	}
    }
    public function ArtistObj() {
	return $this->objDB->Artists()->GetItem($this->ActiveArtist());
    }
    public function AlbumObj() {
	return $this->objDB->Albums()->GetItem($this->Value('album_id'));
    }
    public function FileObj() {
	return $this->objDB->Files()->GetItem($this->Value('id_file'));
    }
}
class clsRequest extends clsDataSet {
    public function TitleObj() {
	return $this->objDB->Titles()->GetItem($this->Value('id_title'));
    }
}

class clsFile extends clsDataSet {
    public function Extn() {
	$strExt = strrchr( $this->Value('filespec'),'.');	// get from the last dot
	$strExt = substr($strExt,1);		// drop the dot
	return $strExt;
    }
}
function NoObject($iVar) {
    if (isset($iVar)) {
	return !is_object($iVar);
    } else {
	return TRUE;
    }
}
