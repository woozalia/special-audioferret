<?php namespace audioFerret;
/*
  NAME: SpecialAudioFerret
  PURPOSE: Special page for accessing AudioFerret
  AUTHOR: Woozalia Staddon
  VERSION:
    2009-03-10 0.0 (Wzl) Started writing
    2009-03-12 0.0 (Wzl) everything working except download/queue
    2009-04-06 0.1 (Wzl) Basic features working; minor fix to prevent use of undeclared variables
    2009-05-29 0.2 (Wzl) Now using /key:val/ internal URL format
	allows direct linking, but makes it more difficult to embed full functionality
	maybe we should support both URL formats? if embedded, use ?key=val, otherwise use /key:val/?
    2009-06-21 0.3 (Wzl) After queueing title, shows title's album/artist listing (so you don't have to backtrack)
    2009-06-22 0.4 (Wzl) Added "most recently played" list, which required some serious table reformatting
    2009-12-31 0.41 (Wzl) option for favicon
    2010-04-05 0.5 (Wzl) separate page for each function
    2012-01-10 0.51 (Wzl) minor bugfixes due to API changes (mostly data.php)
    2015-10-15 0.60 (Wzl) updating code to work with latest Ferreteria modules
    2017-11-29 0.61 (Wzl) more of the same... lots more...
*/
$wgSpecialPages['AudioFerret'] = __NAMESPACE__.'\SpecialAudioFerret'; # tells MW the name of the class to load
$wgExtensionCredits['other'][] = array(
        'name' => 'Special:AudioFerret',
	'url' => 'http://htyp.org/AudioFerret',
        'description' => 'special page for AudioFerret access',
        'author' => 'Woozle Hypertwin Staddon',
	'version' => '0.7 2017-12-01 alpha'
);
$dir = dirname(__FILE__) . '/';
$wgAutoloadClasses['AudioFerret'] = $dir . 'AudioFerret.php'; # Location of the SpecialMyExtension class (Tell MediaWiki to load this file)
$wgExtensionMessagesFiles['AudioFerret'] = $dir . 'AudioFerret.i18n.php'; # Location of a messages filename

//require_once(KFP_LIB.'/config-libs.php');
//require_once(KFP_MW_LIB.'/config-libs.php');
//require_once(KFP_LIB.'/ferreteria/config-libs.php');

/* 2017-12-03 Nothing should be using this anymore. It was used by the deprecated SpecialPageApp class.
if (!defined('KS_CHAR_URL_ASSIGN')) {
    define('KS_CHAR_URL_ASSIGN',':');	// character used for encoding values in wiki-internal URLs
}
*/
if (!defined('KS_CHAR_PATH_SEP')) {
    define('KS_CHAR_PATH_SEP','/');	// character used for encoding values in wiki-internal URLs
}
if (!defined('KS_CHAR_URL_ASSIGN')) {
    define('KS_CHAR_URL_ASSIGN',':');	// character used for encoding values in wiki-internal URLs
}
if (!defined('KS_KEY_PAGENAME')) {
    define('KS_KEY_PAGENAME','do');
}

define('KS_ACTION_AUFER_ARTIST','artist');
define('KS_ACTION_AUFER_ALBUM','album');
define('KS_ACTION_AUFER_TITLE','title');

class SpecialAudioFerret extends \ferreteria\mw\cSpecialPage {

    public function __construct() {
	  parent::__construct( 'AudioFerret' );
    }
    
    function execute( $sPathInfo ) {
	global $wgRequest, $wgOut, $wgUser;
	global $AudioFerretOptions;

	$oApp = cApp::Make();	// make sure the application object exists, and grab it
	//$oApp->MakeKiosk($sPathInfo);
	$oApp->SetPathInfo($sPathInfo);
	$oApp->SetSpecialPage($this);
	
	if (isset($AudioFerretOptions['favicon'])) {
	    $wgOut->addHeadItem('faviconlink', '<link rel="icon" type="image/vnd.microsoft.icon" href="'.$AudioFerretOptions['favicon'].'">' );
	}

	$this->setHeaders();

// Open the AudioFerret database
	$dbAF = $oApp->GetDatabase();
	$dbAF->Open();

// check for action requests
//	$this->GetArgs($par);

	$oPath = $oApp->GetKioskObject()->GetInputObject();

	$strDo = $oPath->GetString('page');
	$id = $oPath->GetInt('id');
	$doDebug = $oPath->GetBool('debug');
	$noHdr = $oPath->GetBool('-hdr');

//	$idArtist = NULL;
//	$idAlbum = NULL;
	$out = NULL;
	switch ($strDo) {
	  case 'artist':
	    //$idArtist = $id;
	    $rcArtist = $oApp->ArtistTable()->GetRecord_forKey($id);
	    if ($rcArtist->HasRows()) {
		$sArtist = $rcArtist->GetFieldValue('name');
		if (!$noHdr) {
		    $out .= "<h2>$sArtist</h2>";
		}
		$htAlbums = $rcArtist->RenderAlbums();
		$htSingles = $rcArtist->RenderTitles_noAlbum();
		if (!is_null($htAlbums)) {
		    $out .= '<h3>Albums</h3>'.$htAlbums;
		}
		if (!empty($htSingles)) {
		    $out .= '<h3>Singles</h3>'.$htSingles;
		}
	    } else {
		$out .= "Artist ID $id not found. This shouldn't happen. SQL=".$rcArtist->sql;
	    }
	    break;
	  case 'album':
	    //$idAlbum = $id;
	    $rcAlbum = $oApp->AlbumTable()->GetRecord_forKey($id);
	    $rcArtist = $oApp->ArtistTable()->GetRecord_forKey($rcAlbum->GetArtistID());
	    $sArtist = $rcArtist->GetNameString();
	    $sAlbum = $rcAlbum->GetNameString();
    	    if (!$noHdr) {
		$out .= '<h3>'.$rcArtist->SelfLink($sArtist).': <i>'.$sAlbum.'</i></h3>';
	    }
	    $out .= $rcAlbum->RenderTitles();
	    break;
	  case 'title':
//	    $idTitle = $id;
	    $rcTitle = $oApp->TitleTable()->GetRecord_forKey($id);
	    $out .= $rc->RenderDetails();
	    break;
	  case 'queue':
	    $idTitle = $id;
	    $dbAF->QueueTitle($id);
	    break;
	  case 'grab':
	    $rcTitle = $oApp->TitleTable()->GetRecord_forKey($id);
	    //$dbAF->GrabTitle($id);
	    $rcTitle->SendDownload();	// send the title's file in downloadable form
	    break;
	  default:
	    $rsArtists = $oApp->ArtistTable()->QueryRecords_withSortings();
	    $ht = $rsArtists->RenderList();
	    if (is_null($ht)) {
		$out .= "Can't retrieve artist list - check database spec!";
	    } else {
		$out .= $ht;
	    }
	}
	$wgOut->addHTML($out);	$out = '';
	$dbAF->Shut();
  }
}
class cApp extends \fcAppStandard {

    // ++ SETUP ++ //
    
    private $fpPathInfo;
    public function SetPathInfo($fp) {
	$this->fpPathInfo = $fp;
    }
    public function GetPathInfo() {
	return $this->fpPathInfo;
    }

    // ++ SETUP ++ //
    // ++ CLASS NAMES ++ //

    protected function GetPageClass() {
	return __NAMESPACE__.'\cPage';
    }
    protected function GetKioskClass() {
	return __NAMESPACE__.'\cKiosk';
    }

    // -- CLASS NAMES -- //
    // ++ OBJECTS ++ //
    
    private $db = NULL;
    // CEMENT
    public function GetDatabase() {
	if (is_null($this->db)) {
	    $this->db = \fcDBOFactory::GetConn(KS_DB_AUFERRET,FALSE);	// connect to AF tables; must succeed
	}
	return $this->db;
    }

    private $mwoSpecial;
    public function SetSpecialPage($mwo) {
	$this->mwoSpecial = $mwo;
    }
    public function GetSpecialPage() {
	return $this->mwoSpecial;
    }

/* 2017-12-03 I think this is redundant now.
    protected function CreateKioskObject($sClass) {
	//$this->SetKioskObject(new $sClass($this->GetPathInfo()));
	$this->SetKioskObject(new $sClass());
    } */
    
    // -- OBJECTS -- //
    // ++ TABLES ++ //
    
    protected function MakeLocalTableWrapper($sClass) {
	return $this->GetDatabase()->MakeTableWrapper(__NAMESPACE__.'\\'.$sClass);
    }
    public function ArtistTable() {
	return $this->MakeLocalTableWrapper('ctArtists');
    }
    public function AlbumTable() {
	return $this->MakeLocalTableWrapper('ctAlbums');
    }
    public function TitleTable() {
	return $this->MakeLocalTableWrapper('ctTitles');
    }
    public function FileTable() {
	return $this->MakeLocalTableWrapper('ctFiles');
    }
    
    // -- TABLES -- //
}
class cKiosk extends \fcMenuKiosk_admin {

    // ++ SETUP ++ //
/*
    public function __construct($sPathInfo,SpecialPage $mwo) {
	$this->SetPathInfo($sPathInfo);
    }

    // -- SETUP -- //
    // ++ VALUES ++ //
        
    private $fp;
    protected function SetPathInfo($fp) {
	$this->fp = $fp;
    }
    protected function GetPathInfo() {
	return $this->fp;
    }
*/    
    // -- VALUES -- //
    // ++ CALCULATIONS ++ //
    
    public function GetInputString() {
	return cApp::Me()->GetPathInfo();
    }
    public function GetBasePath() {
	//global $wgTitle;
	
	$mwoTitle = cApp::Me()->GetSpecialPage()->getPageTitle(FALSE);
	return $mwoTitle->getInternalURL() . '/';	// this may or may not be the best method to use here
    }
    
    // -- CALCULATIONS -- //
}

abstract class ctTableStandard extends \fcTable_keyed_single {

    // ++ SETUP ++ //

    // CEMENT
    public function GetKeyName() {
	return 'id';	// AudioFerret was a very early project, and I hadn't yet settled on "ID"
    }
    
    // -- SETUP -- //
}

class ctArtists extends ctTableStandard implements \fiLinkableTable {
    use \ftLinkableTable;

    /*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Artist');
	  $this->KeyName('id');
	  $this->ClassSng('crArtist');
	  $this->ActionKey('artist');
    } */

    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\crArtist';
    }
    protected function TableName() {
	return 'Artist';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_ARTIST;
    }
    
    // -- SETUP -- //
    // ++ RECORDS ++ //
    
    public function QueryRecords_withSortings() {
	$sqlSelf = $this->SourceString_forSelect();
	$sql = <<<__END__
SELECT `id`,`name`
  FROM $sqlSelf 
UNION SELECT `id`,`sort`
  FROM $sqlSelf
WHERE (NULLIF(`sort`, '') IS NOT NULL)
ORDER BY `name`
__END__;
	return $this->FetchRecords($sql);
    }

    // -- RECORDS -- //
    
    /* 2017-12-02 looks obsolete
    public function RenderList() {
	  global $vgPage;

	  $sql = 'SELECT * FROM qryArtists_index ORDER BY name';
	  $objData = $this->objDB->DataSet($sql,'crArtist');
	  $objData->Table = $this;

	  if ($objData->hasRows()) {
	      $out = '';
	      while ($objData->NextRow()) {
		  if ($out != '') {
		      $out .= '<br>';
		  }
		  $out .= $objData->SelfLink($objData->Value('name'));
	      }
	  } else {
	      $out = "Can't retrieve artist list - check database spec!";
	  }
	  return $out;
    }*/
}
class crArtist extends \fcRecord_standard implements \fiLinkableRecord {
    use \ftLinkableRecord;
    
    // ++ FIELD VALUES ++ //
    
    public function GetNameString() {
	return $this->GetFieldValue('name');
    }
    
    // -- FIELD VALUES -- //
    // ++ WEB UI ++ //
   
    public function RenderList() {
	$out = NULL;
	if ($this->HasRows()) {
	    while ($this->NextRow()) {
		if (!is_null($out)) {
		    $out .= '<br>';
		}
		$out .= $this->SelfLink($this->GetFieldValue('name'));
	    }
	}
	return $out;
    }
    public function RenderAlbums() {
	$t = cApp::Me()->AlbumTable();
	$rs = $t->SelectRecords_forArtist($this->GetKeyValue());
	return $rs->RenderList();
    }
    public function RenderTitles_noAlbum() {
	$t = cApp::Me()->TitleTable();
	$rs = $t->SelectRecords_forArtist_noAlbum($this->GetKeyValue());
	return $rs->RenderList();
    }

    // -- WEB UI -- //
}

class ctAlbums extends ctTableStandard implements \fiLinkableTable {
    use \ftLinkableTable;

    /*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Album');
	  $this->KeyName('id');
	  $this->ClassSng('crAlbum');
	  $this->ActionKey('album');
    }*/
    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\crAlbum';
    }
    protected function TableName() {
	return 'Album';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_ALBUM;
    }
    
    // -- SETUP -- //
    // ++ RECORDS ++ //

    public function SelectRecords_forArtist($idArtist) {
	return $this->SelectRecords('idartist='.$idArtist,'sort');
    }
    
    // -- RECORDS -- //
    //// OLD

    /* 2017-12-02 replacing this
    public function RenderList($iArtist, $iCurrent = NULL,$iNone=NULL) {
	global $vgPage;

	$vgPage->UseHTML();
	//$sql = 'SELECT * FROM '..' WHERE idartist='.$iArtist.' ORDER BY sort';
	$objData = $this->GetData('idartist='.$iArtist,NULL,'sort');
	if ($objData->hasRows()) {
	    $out = '';
	    while ($objData->NextRow()) {
		$strName = $objData->Value('name');
		// square brackets [] confuse the wiki parser; change them to something else
		if ($strName == '') {
		    $strName = '??';
		} else {
		    $strName = str_replace  ('[','&lt;',$strName);
		    $strName = str_replace  (']','&gt;',$strName);
		}
//		 $strName = 'X'.$strName.'X';
		if ($out != '') {
		    $out .= '<br>';
		}
		if ($objData->Value('id') == $iCurrent) {
		    $out .= "<b>$strName</b>";
		} else {
		    //$out .= WikiSelfLink('artist='.$iArtist.'&album='.$objData->id,$strName);
		    //$out .= $spgSpecialPage->SelfLinkID('album','id',$objData->id,$objData->name);
		    $out .= $objData->SelfLink($objData->Value('name'));
		}
	    }
	} else {
	    $out = $iNone;
	}
	return $out;
    } */
}
class crAlbum extends \fcRecord_standard implements \fiLinkableRecord {
    use \ftLinkableRecord;

    // ++ FIELD VALUES ++ //

    public function GetNameString() {
	return $this->GetFieldValue('name');
    }
    public function GetArtistID() {
	return $this->GetFieldValue('idartist');
    }
    
    // -- FIELD VALUES -- //
    // ++ RECORDS ++ //
    
    protected function GetTitleRecords() {
	return cApp::Me()->TitleTable()->SelectRecords_forAlbum($this->GetKeyValue());
    }
    
    // -- RECORDS -- //
    // ++ WEB UI ++ //
    
    public function RenderList() {
	$out = NULL;
	if ($this->HasRows()) {
	    while ($this->NextRow()) {
		$sName = $this->GetNameString();
		if ($sName == '') {
		    $sName = '??';
		} else {
		    // square brackets [] confuse the wiki parser; change them to something else
		    $sName = str_replace  ('[','&lt;',$sName);
		    $sName = str_replace  (']','&gt;',$sName);
		}
//		 $strName = 'X'.$strName.'X';
		if ($out != '') {
		    $out .= '<br>';
		}
		/* 2017-12-03 not sure who ever sets $iCurrent
		if ($this->GetKeyValue() == $iCurrent) {
		    $out .= "<b>$sName</b>";
		} else { */
		    $out .= $this->SelfLink($sName);
		//}
	    }
	}
	return $out;
    }
    public function RenderTitles() {
	$rs = $this->GetTitleRecords();
	return $rs->RenderList();
    }
    
    // -- WEB UI -- //
}

class ctTitles extends ctTableStandard implements \fiLinkableTable {
    use \ftLinkableTable;

/*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Title');
	  $this->KeyName('id');
	  $this->ClassSng('crTitle');
	  $this->ActionKey('title');
    } */
    
    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\crTitle';
    }
    protected function TableName() {
	return 'Title';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_TITLE;
    }
    
    // -- SETUP -- //
    // ++ RECORDS ++ //
    
    public function SelectRecords_forArtist_noAlbum($idArtist) {
	// get title records for given artist not belonging to an album; sort by "track"
	return $this->SelectRecords("(artist_id=$idArtist) AND (album_id IS NULL)",'track');
    }
    public function SelectRecords_forAlbum($idAlbum) {
	// get title records for given album; sort by "track"
	return $this->SelectRecords('album_id='.$idAlbum,'track');
    }

    // -- RECORDS -- //
    
    /* 2017-12-02 replacing this
    public function RenderList($iArtist, $iAlbum = NULL,$iNone=NULL) {
	global $vgPage,$vgOut;

	$vgPage->UseHTML();

	if (!empty($iAlbum)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE album_id='.$iAlbum.' ORDER BY track';
	    $sqlFilt = 'album_id='.$iAlbum;
	} elseif (!empty($iArtist)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE artist_id='.$iArtist.' ORDER BY track';
	    $sqlFilt = 'artist_id='.$iArtist;
	}
//	$objData = $this->DataSet($sql);

	$objData = $this->GetData($sqlFilt);
	$objData->Table = $this;

  	$canGrab = $this->objDB->CanGrab();
	$canQueue = $this->objDB->CanQueue();

	if ($objData->hasRows()) {
	    $out = "\n<table>";
	    while ($objData->NextRow()) {
		$strTrack = $objData->Value('track');
		$strName = $objData->Value('name');
		if ($strTrack == '') {
		    $strTrack = '?';
		}
		$out .= "<tr><td align=right><b>$strTrack</b></td><td>$strName</td><td>";
		//$out .= ' || ';
		$arLink['id'] = $objData->KeyValue();
		if ($canQueue) {
		    //$out .= $spgSpecialPage->SelfLinkID('queue','id',$objData->id,'queue');
		    $arLink['page'] = 'queue';
		    $out .= $vgOut->SelfLink($arLink,'queue','add track to the playing queue');
		}
		if ($canGrab) {	// only show download links for admins
		    if ($canQueue) {
			$out .= '|';
		    }
		    //$out .= ' '.WikiSelfLink('do=grab&track='.$objData->id,'grab');
		    //$out .= $spgSpecialPage->SelfLinkID('grab','id',$objData->id,'grab');
		    $arLink['page'] = 'grab';
		    $out .= $vgOut->SelfLink($arLink,'grab','download the file for this track');
		}
		$out .= '</td></tr>';
	    }
	    $out .= "\n</table>";
	} else {
	    $out = $iNone;
	}
	return $out;
    } */
}
class crTitle extends \fcRecord_standard implements \fiLinkableRecord {
    use \ftLinkableRecord;
//    private $vArtists;

    // ++ FIELD VALUES ++ //

    protected function GetArtistID() {
	return $this->GetFieldValue('artist_id');
    }
    protected function GetAlbumID() {
	return $this->GetFieldValue('album_id');
    }
    protected function GetFileID() {
	return $this->GetFieldValue('id_file');
    }
    protected function GetNameString() {
	return $this->GetFieldValue('name');
    }
    protected function GetTrackString() {
	return $this->GetFieldValue('track');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    protected function HasAlbum() {
	return !is_null($this->GetAlbumID());
    }
    // PURPOSE: kluge to cover for irregular data
    protected function LookupArtistID() {
	$idArtist = $this->GetArtistID();
	if (is_null($idArtist)) {
	    $rcAlbum = $this->GetAlbumRecord();
	    $idArtist = $rcAlbum->GetArtistID();
	}
	return $idArtist;
    }
    // PURPOSE: returns a sane filespec for use with the current title
    public function FriendlySpec($sTemplate,$sSep) {
	if ($this->HasAlbum()) {
	    $rcAlbum = $this->GetAlbumRecord();
	    $sAlbum = $rcAlbum->GetNameString();
	} else {
	    $sAlbum = '';	// no album for this track
	}

	$out = $sTemplate;
	$out = str_replace('<artist>',$this->GetArtistRecord()->GetNameString(),	$out);
	$out = str_replace('<album>',$sAlbum,						$out);
	$out = str_replace('<title>',$this->GetNameString(),				$out);
	$out = str_replace('<track>',$this->GetTrackString(),				$out);
	$out = str_replace('<ext>',$this->GetFileRecord()->GetExtString(),				$out);
	$out = str_replace('<sep>',$sSep,$out);
	return $out;
    }

    // -- FIELD CALCULATIONS -- //
    // ++ RECORDS ++ //
    
    protected function GetFileRecord() {
	return cApp::Me()->FileTable()->GetRecord_forKey($this->GetFileID());
    }
    protected function GetAlbumRecord() {
	return cApp::Me()->AlbumTable()->GetRecord_forKey($this->GetAlbumID());
    }
    protected function GetArtistRecord() {
	$rc = cApp::Me()->ArtistTable()->GetRecord_forKey($this->LookupArtistID());
	return $rc;
    }
   
    // -- RECORDS -- //
    // ++ WEB UI ++ //
    
    public function RenderList() {
	$canQueue = TRUE;	// this was going to be a permissions-based thing... probably needs rethinking
	$canGrab = TRUE;	// same
    
	$out = NULL;
	if ($this->HasRows()) {
	    $out = "\n<table>";
	    while ($this->NextRow()) {
		$sTrack = $this->GetFieldValue('track');
		$sName = $this->GetFieldValue('name');
		if ($sTrack == '') {
		    $sTrack = '?';
		}
		$out .= "<tr><td align=right><b>$sTrack</b></td><td>$sName</td><td>";
		$arLink['id'] = $this->GetKeyValue();
		if ($canQueue) {
		    $arLink = array('page' => 'queue');
		    $out .= $this->SelfLink('queue','add track to the playing queue',$arLink);
		}
		if ($canGrab) {	// only show download links for admins
		    if ($canQueue) {
			$out .= '|';
		    }
		    $arLink = array('page' => 'grab');
		    $out .= $this->SelfLink('grab','download the file for this track',$arLink);
		}
		$out .= '</td></tr>';
	    }
	    $out .= "\n</table>";
	}
	return $out;
    }
    /*
      ACTION: download audio file for the given title
    */
    public function SendDownload() {
	global $wgRequest, $wgOut;
	global $doDebug;

	define( 'MW_NO_OUTPUT_COMPRESSION', 1 );	// this is absolutely vital or the download won't work

	//$objTitle = $this->Titles()->GetItem($iTitle);
	$rcFile = $this->GetFileRecord();

	$fsRel = str_replace('\\', '/', $rcFile->GetFieldValue('filespec'));	// db was originally loaded in Windows
	$fsFull = KFP_AUFERRET_AUDIO.$fsRel;
	$fsEscaped = str_replace ( '"','\\"',$fsFull );

	$sMime = mime_content_type($fsFull);
	$fsNameAs = $this->FriendlySpec('<artist><sep><album><sep><track> <title>.<ext>',' - ');
	$fsNameAs = str_replace ( '"',"''",$fsNameAs);

	$stat = @stat( $fsFull );
	if ( !$stat ) {
	    $wgOut->addWikiText("File $fsFull doesn't seem to be available.");
	} else {
//	    $canGrab = $this->CanGrab();
	    $canGrab = TRUE;	// hard-wired for now
	    if ($canGrab) {
		if ( headers_sent() ) {
			$wgOut->addWikiText ("Internal error: headers already sent; can't send file.");
			return;
		}
		//ob_end_clean();
		if ($doDebug) {
		    $wgOut->addWikiText('* Content-type: '.$strMime,TRUE);
		    $wgOut->addWikiText('* Content-Disposition: attachment; filename="'.$fsNameAs.'"',TRUE);
		    $wgOut->addWikiText('* Content-Length: ' . $stat['size'] );
		    $wgOut->addWikiText('* source filespec: ' . $fsFull );
		} else {
		    header('Content-Description: File Transfer');
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
		    header('Content-type: '.$sMime);
		    //header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');
		    header('Content-Length: ' . $stat['size'] );
// this gets the download dialogue with a reasonable filesize, but no file:
		    ob_end_flush();

		    readfile($fsFull);
		    exit;
		}
	    } else {
		$wgOut->AddWikiText('File information:<br>');
		$wgOut->AddWikiText("\n* Content-type: $strMime");
		$wgOut->AddWikiText("\n* Content-Disposition: attachment; filename=\"".$fsNameAs.'"');
		$wgOut->AddWikiText("\n* Content-Length: ".$stat['size'] );
  		//readfile($fsFull);
	    }
	}
    }

    // -- WEB UI -- //
/* 2017-12-03 retired unless it turns out they're actually needed

    public function LongName() {
	$out = '&ldquo;'.$this->Value('name').'&rdquo; by '.$this->ArtistObj()->Value('name');
	return $out;
    }
    public function ActiveArtist() {
	$idArtist = $this->Value('artist_id');
	if (is_null($idArtist)) {
	    return $this->AlbumObj()->Value('idartist');
	} else {
	    return $idArtist;
	}
    }
    public function ArtistObj() {
	return $this->Engine()->Artists()->GetItem($this->ActiveArtist());
    }
    public function AlbumObj() {
	if ($this->HasAlbum()) {
	    return $this->Engine()->Albums()->GetItem($this->AlbumID());
	} else {
	    return NULL;
	}
    }
    public function FileObj() {
	return $this->objDB->Files()->GetItem($this->Value('id_file'));
    }
*/
}

class ctFiles extends ctTableStandard {

    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\crFile';
    }
    protected function TableName() {
	return 'Files';
    }
    
    // -- SETUP -- //
}
class crFile extends \fcRecord_standard {
    
    // ++ FIELD CALCULATION ++ //
    
    public function GetExtString() {
	$sExt = strrchr($this->GetFieldValue('filespec'),'.');	// get from the last dot
	$sExt = substr($sExt,1);		// drop the dot
	return $sExt;
    }

    // -- FIELD CALCULATION -- //
}

/* 2017-11-29 not sure what these should descend from; wait until we need them
class clsRequest extends \clsDataSet {
    public function TitleObj() {
	return $this->objDB->Titles()->GetItem($this->Value('id_title'));
    }
}

class clsFile extends \clsDataSet {
    public function Extn() {
	$strExt = strrchr( $this->Value('filespec'),'.');	// get from the last dot
	$strExt = substr($strExt,1);		// drop the dot
	return $strExt;
    }
}
*/
