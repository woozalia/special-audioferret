<?php namespace audioFerret;
/*
  NAME: SpecialAudioFerret
  PURPOSE: Special page for accessing AudioFerret
  AUTHOR: Woozle (Nick) Staddon
  VERSION:
    2009-03-10 0.0 (Wzl) Started writing
    2009-03-12 0.0 (Wzl) everything working except download/queue
    2009-04-06 0.1 (Wzl) Basic features working; minor fix to prevent use of undeclared variables
    2009-05-29 0.2 (Wzl) Now using /key:val/ internal URL format
	allows direct linking, but makes it more difficult to embed full functionality
	maybe we should support both URL formats? if embedded, use ?key=val, otherwise use /key:val/?
    2009-06-21 0.3 (Wzl) After queueing title, shows title's album/artist listing (so you don't have to backtrack)
    2009-06-22 0.4 (Wzl) Added "most recently played" list, which required some serious table reformatting
    2009-12-31 0.41 (Wzl) option for favicon
    2010-04-05 0.5 (Wzl) separate page for each function
    2012-01-10 0.51 (Wzl) minor bugfixes due to API changes (mostly data.php)
    2015-10-15 0.60 (Wzl) updating code to work with latest Ferreteria modules
    2017-11-29 0.61 (Wzl) more of the same... lots more...
*/
$wgSpecialPages['AudioFerret'] = 'audioFerret\SpecialAudioFerret'; # tells MW the name of the class to load
$wgExtensionCredits['other'][] = array(
        'name' => 'Special:AudioFerret',
	'url' => 'http://htyp.org/AudioFerret',
        'description' => 'special page for AudioFerret access',
        'author' => 'Woozle (Nick) Staddon',
	'version' => '0.61 2017-11-29 alpha'
);
$dir = dirname(__FILE__) . '/';
$wgAutoloadClasses['AudioFerret'] = $dir . 'AudioFerret.php'; # Location of the SpecialMyExtension class (Tell MediaWiki to load this file)
$wgExtensionMessagesFiles['AudioFerret'] = $dir . 'AudioFerret.i18n.php'; # Location of a messages filename

//require_once(KFP_LIB.'/config-libs.php');
//require_once(KFP_MW_LIB.'/config-libs.php');
//require_once(KFP_LIB.'/ferreteria/config-libs.php');

if (!defined('KS_CHAR_URL_ASSIGN')) {
    define('KS_CHAR_URL_ASSIGN',':');	// character used for encoding values in wiki-internal URLs
}
if (!defined('KS_KEY_PAGENAME')) {
    define('KS_KEY_PAGENAME','do');
}

define('KS_ACTION_AUFER_ARTIST','artist');
define('KS_ACTION_AUFER_ALBUM','album');
define('KS_ACTION_AUFER_TITLE','title');

class SpecialAudioFerret extends \SpecialPageApp {

    public function __construct() {
	  global $wgMessageCache;
	  global $spgSpecialPage;
	  global $vgPage;

	  $spgSpecialPage = $this;

	  parent::__construct( 'AudioFerret' );
	  $vgPage = $this;

//	  $this->includable( TRUE );	// sure, why not?
	  //$wgMessageCache->addMessage('audioferret', 'AudioFerret access');

    }
    /*
    FUNCTION: SelfLinkID
    PURPOSE: self-link for always passing an ID
    */
    function SelfLinkID($iDo,$iKey,$iVal,$iText) {
    //    return '[[{{FULLPAGENAME}}/do'.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	//return '[[Special:AudioFerret/'.KS_KEY_PAGENAME.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	//return '[[{{FULLPAGENAME}}/page'.KS_CHAR_URL_ASSIGN.$iPage.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
	return '[[Special:'.$this->name().'/'.KS_KEY_PAGENAME.KS_CHAR_URL_ASSIGN.$iDo.'/'.$iKey.KS_CHAR_URL_ASSIGN.$iVal.'|'.$iText.']]';
    }
    function execute( $par ) {
	global $wgRequest, $wgOut, $wgUser;
	global $vgPage;
	global $doDebug;
	global $AudioFerretOptions;

	\fcApp_MW::Make();	// make sure the application object exists
	
	$vgPage->UseHTML();

	if (isset($AudioFerretOptions['favicon'])) {
	    $wgOut->addHeadItem('faviconlink', '<link rel="icon" type="image/vnd.microsoft.icon" href="'.$AudioFerretOptions['favicon'].'">' );
	}

	$this->setHeaders();

// Open the AudioFerret database
	$dbAF = new clsAFData(KS_DB_AUFERRET);
	$dbAF->Open();

// check for action requests
	$this->GetArgs($par);

	$strDo = $this->Arg('page');
	$id = $this->Arg('id');
	$doDebug = $this->Arg('debug');
	$noHdr = $this->Arg('-hdr');

	$idArtist = NULL;
	$idAlbum = NULL;
	$out = NULL;
	switch ($strDo) {
	  case 'artist':
	    $idArtist = $id;
	    $objArtist = $dbAF->Artists()->GetItem($id);
	    $strArtist = $objArtist->Value('name');
	    if (!$noHdr) {
		$out .= '<h2>'.$strArtist.'</h2>';
	    }
	    $outAlb = $dbAF->GetAlbums($idArtist);
	    $outSng = $dbAF->GetTitles($idArtist);
	    if (!empty($outAlb)) {
		$out .= '<h3>Albums</h3>'.$outAlb;
	    }
	    if (!empty($outSng)) {
		$out .= '<h3>Singles</h3>'.$outSng;
	    }
	    break;
	  case 'album':
	    $idAlbum = $id;
	    $objAlbum = $dbAF->Albums()->GetItem($id);
	    $objArtist = $dbAF->Artists()->GetItem($objAlbum->ArtistID());
	    $strArtist = $objArtist->Value('name');
	    $strAlbum = $objAlbum->Value('name');
    	    if (!$noHdr) {
		$out .= '<h3>'.$objArtist->SelfLink($strArtist).': <i>'.$strAlbum.'</i></h3>';
	    }
	    $out .= $dbAF->GetTitles(NULL,$idAlbum);
	    break;
	  case 'title':
	    $idTitle = $id;
	    $out .= $dbAf->RenderTitle($idTitle);
	    break;
	  case 'queue':
	    $idTitle = $id;
	    $dbAF->QueueTitle($id);
	    break;
	  case 'grab':
	    $dbAF->GrabTitle($id);
	    break;
	  default:
	    $out .= $dbAF->GetArtists($idArtist);
	}
/**/
/*
	if (isset($idTitle)) {
	  if (!is_null($idTitle)) {
	      $sql = 'SELECT * FROM Titles WHERE id='.$idTitle;
	      $objTitle = $dbAF->DataSet($sql,'clsTitle');
	      $objTitle->NextRow();
	      $idAlbum = $objTitle->album_id;
	  }
	}
	if (!is_null($idAlbum)) {
	    $sql = 'SELECT * FROM Albums WHERE id='.$idAlbum;
	    $objAlbum = $dbAF->DataSet($sql,'clsAlbum');
	    $objAlbum->NextRow();
	    $strAlbum = $objAlbum->NameLink();
	    $idArtist = $objAlbum->idartist;
	}
	if (!is_null($idArtist)) {
	    $sql = 'SELECT * FROM Artists WHERE id='.$idArtist;
	    $objArtist = $dbAF->DataSet($sql,'clsArtist');
	    $objArtist->NextRow();
	    $strArtist = $objArtist->NameLink();
	}
// collect data for the display
	$outArtists = $dbAF->GetArtists($idArtist);
	if (!is_null($idArtist)) {
	    $outAlbums = $dbAF->GetAlbums($idArtist,$idAlbum);
	    $outTitles = $dbAF->GetTitles($idArtist,$idAlbum);
	} else {
	    $outAlbums = NULL;
	    $outTitles = NULL;
	}

	$outPlayed = $dbAF->ListPlayed(10);

// display the current listing(s)
	$out = $doDebug?"DEBUG mode\n":'';
	$out .= "__NOTOC__\n{| border=0";
	$outHtArtist = '';
	$outHtAlbum = '';
	if ($idArtist != '') {
	    $outHtArtist = "'''$strArtist'''\n==Albums==\n";
	    if ($idAlbum) {
		$outHtAlbum = "'''$strAlbum'''\n==Tracks==\n";
	    }
	}
	$outRtPanel = "\n{|\n|-\n|\n{|\n|-\n| valign=top | $outHtArtist$outAlbums \n| valign=top | $outHtAlbum$outTitles\n|}\n|}";
	$outRtPanel .= "\n==most recently played==\n$outPlayed";

	$out .= "\n|-\n| valign=top |\n==Artists==\n$outArtists";
	$out .= "\n| valign=top | $outRtPanel";

	$out .= "\n|}";

	global $wgParserConf;
	$objParser = new StubObject( 'wgParser', $wgParserConf['class'], array( $wgParserConf ) );
*/

	//$wgOut->addWikiText($out,TRUE);	$out = '';
	$wgOut->addHTML($out);	$out = '';
	$dbAF->Shut();
  }
    /*-----
      PURPOSE: Parses variable arguments from the URL
	The URL is formatted as a series of arguments /arg=val/arg=val/..., so that we can always refer directly
	  to any particular item as a wiki page title while also not worrying about hierarchy/order.
    */
/*
    private function GetArgs($par) {
	$args_raw = split('/',$par);
	foreach($args_raw as $arg_raw) {
	    if (strpos($arg_raw,KS_CHAR_URL_ASSIGN) !== FALSE) {
		list($key,$val) = split(KS_CHAR_URL_ASSIGN,$arg_raw);
		$this->args[$key] = $val;
	    } else {
		$this->args[$arg_raw] = TRUE;
	    }
	}
    }
*/
}
abstract class ctTableStandard extends \fcTable_keyed_single {

    // ++ SETUP ++ //

    // CEMENT
    public function GetKeyName() {
	return 'id';	// AudioFerret was a very early project, and I hadn't yet settled on "ID"
    }
    
    // -- SETUP -- //
}

class ctArtists extends ctTableStandard {
    use \ftLinkableTable;

    /*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Artist');
	  $this->KeyName('id');
	  $this->ClassSng('crArtist');
	  $this->ActionKey('artist');
    } */

    // ++ SETUP ++ //

    protected function SingularName() {
	return 'crArtist';
    }
    protected function TableName() {
	return 'Artist';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_ARTIST;
    }
    
    // -- SETUP -- //

    public function RenderList() {
	  global $vgPage;

	  $sql = 'SELECT * FROM qryArtists_index ORDER BY name';
	  $objData = $this->objDB->DataSet($sql,'crArtist');
	  $objData->Table = $this;

	  if ($objData->hasRows()) {
	      $out = '';
	      while ($objData->NextRow()) {
		  if ($out != '') {
		      $out .= '<br>';
		  }
		  $out .= $objData->SelfLink($objData->Value('name'));
	      }
	  } else {
	      $out = "Can't retrieve artist list - check database spec!";
	  }
	  return $out;
    }
}
class crArtist extends \fcRecord_standard {
    use \ftLinkableRecord;
/*
    // BOILERPLATE section //

    public function AdminLink($iText=NULL,$iPopup=NULL,array $iarArgs=NULL) {
	return clsAdminData_helper::_AdminLink($this,$iText,$iPopup,$iarArgs);
    }
*/
// DYNAMIC section //
}

class ctAlbums extends ctTableStandard {
    use \ftLinkableTable;

    /*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Album');
	  $this->KeyName('id');
	  $this->ClassSng('crAlbum');
	  $this->ActionKey('album');
    }*/
    // ++ SETUP ++ //

    protected function SingularName() {
	return 'crAlbum';
    }
    protected function TableName() {
	return 'Album';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_ALBUM;
    }
    
    // -- SETUP -- //
    
    public function RenderList($iArtist, $iCurrent = NULL,$iNone=NULL) {
	global $vgPage;

	$vgPage->UseHTML();
	//$sql = 'SELECT * FROM '..' WHERE idartist='.$iArtist.' ORDER BY sort';
	$objData = $this->GetData('idartist='.$iArtist,NULL,'sort');
	if ($objData->hasRows()) {
	    $out = '';
	    while ($objData->NextRow()) {
		$strName = $objData->Value('name');
		// square brackets [] confuse the wiki parser; change them to something else
		if ($strName == '') {
		    $strName = '??';
		} else {
		    $strName = str_replace  ('[','&lt;',$strName);
		    $strName = str_replace  (']','&gt;',$strName);
		}
//		 $strName = 'X'.$strName.'X';
		if ($out != '') {
		    $out .= '<br>';
		}
		if ($objData->Value('id') == $iCurrent) {
		    $out .= "<b>$strName</b>";
		} else {
		    //$out .= WikiSelfLink('artist='.$iArtist.'&album='.$objData->id,$strName);
		    //$out .= $spgSpecialPage->SelfLinkID('album','id',$objData->id,$objData->name);
		    $out .= $objData->SelfLink($objData->Value('name'));
		}
	    }
	} else {
	    $out = $iNone;
	}
	return $out;
    }
}
class crAlbum extends \fcRecord_standard {
    use \ftLinkableRecord;
/*
// BOILERPLATE section //

    public function AdminLink($iText=NULL,$iPopup=NULL,array $iarArgs=NULL) {
	return clsAdminData_helper::_AdminLink($this,$iText,$iPopup,$iarArgs);
    }
*/
// DYNAMIC section //

    public function ArtistID() {
	return $this->Value('idartist');
    }
}

class ctTitles extends ctTableStandard {

/*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Title');
	  $this->KeyName('id');
	  $this->ClassSng('crTitle');
	  $this->ActionKey('title');
    } */
    
    // ++ SETUP ++ //

    protected function SingularName() {
	return 'crTitle';
    }
    protected function TableName() {
	return 'Title';
    }
    public function GetActionKey() {
	return KS_ACTION_AUFER_TITLE;
    }
    
    // -- SETUP -- //
    
    public function RenderList($iArtist, $iAlbum = NULL,$iNone=NULL) {
	global $vgPage,$vgOut;

	$vgPage->UseHTML();

	if (!empty($iAlbum)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE album_id='.$iAlbum.' ORDER BY track';
	    $sqlFilt = 'album_id='.$iAlbum;
	} elseif (!empty($iArtist)) {
//	    $sql = 'SELECT * FROM '.$this->Titles()->Name().' WHERE artist_id='.$iArtist.' ORDER BY track';
	    $sqlFilt = 'artist_id='.$iArtist;
	}
//	$objData = $this->DataSet($sql);

	$objData = $this->GetData($sqlFilt);
	$objData->Table = $this;

  	$canGrab = $this->objDB->CanGrab();
	$canQueue = $this->objDB->CanQueue();

	if ($objData->hasRows()) {
	    $out = "\n<table>";
	    while ($objData->NextRow()) {
		$strTrack = $objData->Value('track');
		$strName = $objData->Value('name');
		if ($strTrack == '') {
		    $strTrack = '?';
		}
		$out .= "<tr><td align=right><b>$strTrack</b></td><td>$strName</td><td>";
		//$out .= ' || ';
		$arLink['id'] = $objData->KeyValue();
		if ($canQueue) {
		    //$out .= $spgSpecialPage->SelfLinkID('queue','id',$objData->id,'queue');
		    $arLink['page'] = 'queue';
		    $out .= $vgOut->SelfLink($arLink,'queue','add track to the playing queue');
		}
		if ($canGrab) {	// only show download links for admins
		    if ($canQueue) {
			$out .= '|';
		    }
		    //$out .= ' '.WikiSelfLink('do=grab&track='.$objData->id,'grab');
		    //$out .= $spgSpecialPage->SelfLinkID('grab','id',$objData->id,'grab');
		    $arLink['page'] = 'grab';
		    $out .= $vgOut->SelfLink($arLink,'grab','download the file for this track');
		}
		$out .= '</td></tr>';
	    }
	    $out .= "\n</table>";
	} else {
/*
	    if ($iAlbum) {
		$out = "No titles for this album";
	    } else {
		$out = "No singles for this artist";
	    }
*/
	    $out = $iNone;
	}
	return $out;
    }
}
class crTitle extends \fcRecord_standard {
    private $vArtists;

    // ++ DATA FIELDS ++ //

    protected function AlbumID() {
	return $this->Value('album_id');
    }
    protected function HasAlbum() {
	return !is_null($this->AlbumID());
    }

    // -- DATA FIELDS -- //

    public function FriendlySpec($iTemplate,$iSep) {
	if ($this->HasAlbum()) {
	    $oAlbum = $this->AlbumObj();
	    $sAlbum = $oAlbum->Value('name');
	} else {
	    $sAlbum = '';	// no album for this track
	}

	$out = $iTemplate;
	$out = str_replace('<artist>',$this->ArtistObj()->Value('name'),	$out);
	$out = str_replace('<album>',$sAlbum,	$out);
	$out = str_replace('<title>',$this->Value('name'),		$out);
	$out = str_replace('<track>',$this->Value('track'),		$out);
	$out = str_replace('<ext>',$this->FileObj()->Extn(),	$out);
	$out = str_replace('<sep>',$iSep,$out);
	return $out;
    }
    public function LongName() {
	$out = '&ldquo;'.$this->Value('name').'&rdquo; by '.$this->ArtistObj()->Value('name');
	return $out;
    }
    public function ActiveArtist() {
	$idArtist = $this->Value('artist_id');
	if (is_null($idArtist)) {
	    return $this->AlbumObj()->Value('idartist');
	} else {
	    return $idArtist;
	}
    }
    public function ArtistObj() {
	return $this->Engine()->Artists()->GetItem($this->ActiveArtist());
    }
    public function AlbumObj() {
	if ($this->HasAlbum()) {
	    return $this->Engine()->Albums()->GetItem($this->AlbumID());
	} else {
	    return NULL;
	}
    }
    public function FileObj() {
	return $this->objDB->Files()->GetItem($this->Value('id_file'));
    }
}

/* 2017-11-29 not sure what these should descend from; wait until we need them
class clsRequest extends \clsDataSet {
    public function TitleObj() {
	return $this->objDB->Titles()->GetItem($this->Value('id_title'));
    }
}

class clsFile extends \clsDataSet {
    public function Extn() {
	$strExt = strrchr( $this->Value('filespec'),'.');	// get from the last dot
	$strExt = substr($strExt,1);		// drop the dot
	return $strExt;
    }
}
*/