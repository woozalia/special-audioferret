<?php
/*
  PURPOSE: old class being removed from AudioFerret.php
*/
class clsAFData extends fcDatabase {
/*==============
| STATIC SECTION
*/
    //const ksTbl_Artists = 'Artists';
    //const ksTbl_Albums = 'Albums';
    //const ksTbl_Titles = 'Titles';
    const ksTbl_Files = 'Files';
    const ksTbl_Queue = 'Tracks_Queued';
    const ksTbl_Played = 'Tracks_Played';

    private static $objArtists;
    private static $objAlbums;
    private static $objTitles;
    private static $objFiles;
/*===============
| DYNAMIC SECTION
*/
/*
    protected function Make($iName) {
	if (!isset($this->arObjs[$iName])) {
	    $this->arObjs[$iName] = new $iName($this);
	}
	return $this->arObjs[$iName];
    }
*/
    public function Artists() {
	return $this->Make('clsAuFerArtists');
    }
    public function Albums() {
	return $this->Make('clsAuFerAlbums');
    }
    public function Titles() {
	return $this->Make('clsAuFerTitles');
    }
/*
    public function Artists() {
	if (NoObject(self::$objArtists)) {
	    $obj = new clsTable($this,self::ksTbl_Artists,'id','clsArtist');
	      $obj->Name(self::ksTbl_Artists);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsArtist');
	    self::$objArtists = $obj;
	}
	return self::$objArtists;
    }
    public function Albums() {
	if (NoObject(self::$objAlbums)) {
	    $obj = new clsTable($this);
	      $obj->Name(self::ksTbl_Albums);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsAlbum');
	      self::$objAlbums = $obj;
	}
	return self::$objAlbums;
    }
    public function Titles() {
	if (NoObject(self::$objTitles)) {
	    $obj = new clsTable($this);
	      $obj->Name(self::ksTbl_Titles);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsTitle');
	      self::$objTitles = $obj;
	}
	return self::$objTitles;
    }
*/
    public function Files() {
	if (NoObject(self::$objFiles)) {
	    $obj = self::$objFiles = new clsTable($this);
	      $obj->Name(self::ksTbl_Files);
	      $obj->KeyName('id');
	      $obj->ClassSng('clsFile');
	}
	return self::$objFiles;
    }

    public function GetArtists($iCurrent = NULL) {
	$out = $this->Artists()->RenderList();
	return $out;
    }
    public function GetAlbums($iArtist, $iCurrent = NULL) {
	$out = $this->Albums()->RenderList($iArtist,$iCurrent);
	return $out;
    }
    public function GetTitles($iArtist, $iAlbum = NULL,$iNone=NULL) {
	$out = $this->Titles()->RenderList($iArtist,$iAlbum,$iNone);
	return $out;
    }
    /*-----
      ACTION: Display information about the title
    */
    public function RenderTitle($iTitle) {
    }
    /*-----
      ACTION: Add title to playing queue
    */
    public function QueueTitle($iTitle) {
	global $wgUser, $wgOut, $wgSitename;

	$sql = 'INSERT INTO `'.self::ksTbl_Queue.'` (id_title,when_requested,user,station) VALUES('
	  .$iTitle.',NOW(),"'
	  .$wgUser->getName()
	   .'","'.$wgSitename.'")';
	$this->Exec($sql);
	$wgOut->addWikiText('Added '.$this->Titles()->GetItem($iTitle)->LongName());
    }
    public function GrabTitle($iTitle) {
    /*
      ACTION: download audio file for the given title
    */
	global $wgRequest, $wgOut;
	global $doDebug;

	define( 'MW_NO_OUTPUT_COMPRESSION', 1 );	// this is absolutely vital or the download won't work

	$objTitle = $this->Titles()->GetItem($iTitle);
	$objFile = $this->Files()->GetItem($objTitle->Value('id_file'));

	$fsRel = str_replace('\\', '/', $objFile->Value('filespec'));	// db was originally loaded in Windows
	$fsFull = KFP_AUFERRET_AUDIO.$fsRel;
	$fsEscaped = str_replace ( '"','\\"',$fsFull );

	$strMime = mime_content_type($fsFull);
	$fsNameAs = $objTitle->FriendlySpec('<artist><sep><album><sep><track> <title>.<ext>',' - ');
	$fsNameAs = str_replace ( '"',"''",$fsNameAs);

/*
	$wgOut->addWikiText(' Content-type: '.$strMime);
	$wgOut->addWikiText(' Content-Disposition: attachment; filename="'.$fsNameAs.'"');
	$wgOut->addWikiText(' FILENAME ESCAPED:'.$fsEscaped);
/*/
//	    $wgRequest->response()->header('Content-type: '.$strMime);
//	    $wgRequest->response()->header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');

	//header('Content-type: text/html');
	//echo "\nSending $fsNameAs...";
// */
	$stat = @stat( $fsFull );
	if ( !$stat ) {
	    $wgOut->addWikiText("File $fsFull doesn't seem to be available.");
	} else {
	    $canGrab = $this->CanGrab();
	    //$canGrab = TRUE;
	    if ($canGrab) {
		if ( headers_sent() ) {
			$wgOut->addWikiText ("Headers already sent; can't send file.");
			return;
		}
		//ob_end_clean();
		if ($doDebug) {
		  $wgOut->addWikiText('* Content-type: '.$strMime,TRUE);
		  $wgOut->addWikiText('* Content-Disposition: attachment; filename="'.$fsNameAs.'"',TRUE);
		  $wgOut->addWikiText('* Content-Length: ' . $stat['size'] );
		  $wgOut->addWikiText('* source filespec: ' . $fsFull );
		} else {
		  header('Content-Description: File Transfer');
		  header('Content-Transfer-Encoding: binary');
		  header('Expires: 0');
		  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		  header('Pragma: public');
		  header('Content-type: '.$strMime);
		  //header('Content-Type: application/octet-stream');
		  header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');
		  header('Content-Length: ' . $stat['size'] );
// this gets the download dialogue with a reasonable filesize, but no file:
		  ob_end_flush();
//		readfile( $fsFull );
// this is sample code from php.net:
//		ob_clean();
//		flush();

		  readfile($fsFull);
		  exit;
		}
//	    echo "\n";
	    //passthru ( 'cat "'.$fsEscaped.'"');
//	    header('Content-type: text/html');
	    //header('Content-type: '.$strMime);
	    //header('Content-Disposition: attachment; filename="'.$fsNameAs.'"');
	    //die();
/**/
	    } else {
	      $wgOut->AddWikiText('File information:<br>');
	      $wgOut->AddWikiText("\n* Content-type: $strMime");
	      $wgOut->AddWikiText("\n* Content-Disposition: attachment; filename=\"".$fsNameAs.'"');
	      $wgOut->AddWikiText("\n* Content-Length: ".$stat['size'] );
  		//readfile($fsFull);
	    }
	}
    }
    public function ListQueued() {
	$sql = 'SELECT * FROM '.self::ksTbl_Queue.' ORDER BY id';
	$objRow = $this->DataSet($sql);
	if ($objRow->hasRows()) {
	    $out = "\n{|";
	    while ($objRow->NextRow()) {
		$objTitle = $objRow->TitleObj();
		$out .= "\n|-\n|".$objTitle->LongName().' || '.$objRow->when_requested;
	    }
	    $out .= "\n|}";
	    return $out;
	} else {
	    return 'no tracks played yet';
	}
    }
    public function ListPlayed($iQty) {
	$sql = 'SELECT * FROM '.self::ksTbl_Played.' ORDER BY id DESC LIMIT '.$iQty;
	$objRow = $this->DataSet($sql,'clsRequest');
	if ($objRow->hasRows()) {
	    $out = "\n{|";
	    while ($objRow->NextRow()) {
		$objTitle = $objRow->TitleObj();
		$out .= "\n|-\n|".$objTitle->LongName().' || '.$objRow->when_started;
	    }
	    $out .= "\n|}";
	    return $out;
	} else {
	    return NULL;
	}
    }
    public function CanGrab() {
	global $wgUser;

  	return $wgUser->isAllowed('editinterface');
    }
    public function CanQueue() {
	global $wgUser;

	return $wgUser->isLoggedIn();
    }
}
