<?php
/**
 * Internationalisation (text bits) for AudioFerret
 *
 * @ingroup Extensions
 */
$messages = array();
 
/** English
 * @author <Woozle>
 */
$messages['en'] = array(
        'audioferret' => 'AudioFerret', // this is how Special:SpecialPages displays the link to this page
        'audioferret-desc' => "jukebox",
);
 
/** Message documentation
 * @author <your username>
 */
$messages['qqq'] = array(
        'audioferret-desc' => "{{desc}}",
);
